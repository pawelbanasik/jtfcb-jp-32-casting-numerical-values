
public class App {

	public static void main(String[] args) {
	
		byte byteValue = 20;
		short shortValue = 55;
		int intValue = 888;
		long longValue = 23355;
		
		// trzeba dac f na koncu bo error bedzie
		float floatValue = 8834.3f;
		
		// alternatywny syntax
		// zamiast tej f na koncu float w nawiasie
		float floatValue2 = (float)99.3;
		double doubleValue = 32.4;
		
		System.out.println(Byte.MAX_VALUE);
		
		
		
		// konwersja z long na int
		// typ do ktorego castuje ten long to int
		// nie da sie tak
		// intValue = longValue;
		// dlatego tak jest bo jest ryzyko bledu bo long nie zmiesci sie w int
		// dlatego musisz castowac tym nawiasem
		// pokazujesz Javie ze wiesz co robisz
		intValue = (int)longValue;
		
		// daje mi long value
		System.out.println(intValue);
		
		
		// tu sie udalo
		// Java zrobila bez castowania sama
		// nie ma errora
		doubleValue = intValue;
		System.out.println(doubleValue);
		
		
		
		// tu musze castowac
		intValue = (int)floatValue;
		System.out.println(intValue);
		
		
		//zrobil zbyt duzy byte
		// max jest 127
		// castuje liczbe do odpowiedniego typu
		// trzeba z tym uwazac
		// to nie dziala tak jak chcemy
		// taki blad mozemy zrobic i bedzie problem
		byteValue = (byte)128; 
		// przeskoczylo o jeden na minus
		System.out.println(byteValue);
 	}

}
